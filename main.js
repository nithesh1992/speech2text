
var final_transcript = '';
var recognizing = false;
var ignore_onend;
var start_timestamp;
var info_upgrade = 'This Project is not supported by your browser, Upgrade to <a href="//www.google.com/chrome">Chrome</a> version 25 or later';
var info_start = 'Click "Start" and begin speaking.';
var info_blocked = 'Permission to use microphone is blocked. To change, go to chrome://settings/contentExceptions#media-stream'
var info_denied = 'Permission to use microphone was denied.';
var info_allow = 'Click the "Allow" button above to enable your microphone.';
var info_speak_now = 'Speak Up';
var info_no_microphone = 'No microphone was found';
var info_no_speech = 'No speech was detected. You may need to adjust your microphone settings';
var two_line = /\n\n/g;
var one_line = /\n/g;
var first_char = /\S/;

window.onload = function() {
      
          var content = [
            { title: 'NN: Singular Noun' },
            { title: 'NNP: Singular Proper Noun' },
            { title: 'NNPA: Noun, Acronym' },
            { title: 'NNAB: Noun, Abbreviation' },
            { title: 'NNPS: Plural Proper Noun ' },
            { title: 'NNS: Plural Noun' },
            { title: 'NNO: Possessive Noun' },
            { title: 'NG : Gerund Noun'},
            { title: 'PRP :Personal Pronoun'},
            { title: 'PP : Possessive Pronoun'},

            { title: 'VB : Verb, generic'},
            { title: 'VBD : Past-tense verb'},
            { title: 'VBN : Past-participle verb'},
            { title: 'VBP : Infinitive verb'},
            { title: 'VBZ : Present-tense verb'},
            { title: 'VBF : Future-tense verb'},
            { title: 'CP : Copula'},
            { title: 'VBG : Gerund Verb'},
            { title: 'JJ : Adjective, Generic'},
            { title: 'JJR : Comparative Adjective'},
            { title: 'JJS : Superlative adjective'},
            { title: 'RB : Adverb'},
            { title: 'RBR : Comparative adverb'},
            { title: 'RBS : Superlative adverb'},
            { title: 'FW : Foreign Word'},
            { title: 'IN : Preposition'},
            { title: 'MD : Modal Verb'},
            { title: 'CC : Co-ordating conjunction'},
            { title: 'DT : Determiner'},
            { title: 'UH : Interjection'},
            { title: 'EX : Existential there'},
            { title: 'CD : Cardinal value'},
            { title: 'DA : Date'},
            { title: 'NU : Number'}


        ];
        $('.ui.dropdown').dropdown();
        $('.ui.search')
            .search({
                type: 'standard',
                source: content,
                searchFields : ['title']
            });
    }



// Speech Recognition
if (!('webkitSpeechRecognition' in window)) {
    message.innerHTML = 'Web Speech API is not supported by this browser. Upgrade to <a href="//www.google.com/chrome">Chrome</a> version 25 or later.';
} else {
    var recognition = new webkitSpeechRecognition();
    var brecognition = new webkitSpeechRecognition();
    recognition.continuous = true;
    brecognition.continuous = true;
    recognition.interimResults = true;
    brecognition.interimResults = true;

    recognition.onstart = function () {
        recognizing = true;
        message.innerHTML = 'Speak now.';
        bisbn_button.innerHTML = 'Hearing';
    };

     brecognition.onstart = function () {
        recognizing = true;
        bmessage.innerHTML = 'Speak now.';
        talk_button.innerHTML = 'Hearing';
    };

    recognition.onresult = function (event) {
        var interim_transcript = '';
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                final_transcript += event.results[i][0].transcript;
            } else {
                interim_transcript += event.results[i][0].transcript;
            }
        }
        final_span.innerHTML = final_transcript;
        interim_span.innerHTML = interim_transcript;
        var s = nlp.pos(final_transcript).sentences[0];
        parse_span.innerHTML = nlp.pos(final_transcript, {}).tags() ;
        tense_span.innerHTML = nlp.pos(final_transcript).tense();

        ngram_span.innerHTML = nGrams(final_transcript, 2) ;
        

    };

        brecognition.onresult = function (event) {
        var binterim_transcript = '';
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                bfinal_transcript += event.results[i][0].transcript;
            } else {
                binterim_transcript += event.results[i][0].transcript;
            }
        }
        // bfinal_span.innerHTML = bfinal_transcript;
        binterim_span.innerHTML = binterim_transcript;
        document.getElementById("ptext").placeholder = bfinal_transcript;

        // setTimeout( getBookbytitle(bfinal_transcript), 2000 );
            


    };

    recognition.onend = function () {

        recognizing = false;
        if (ignore_onend) {
            return;
        }
        // speechMyText(final_transcript);
        if (!final_transcript) {
            message.innerHTML = 'Click "Start" and begin speaking.';
            talk_button.innerHTML = 'Start';
            return;
        }
    };

        brecognition.onend = function () {

        recognizing = false;
        if (bignore_onend) {
            return;
        }
        // speechMyText(final_transcript);
        getBookbytitle(bfinal_transcript);
        if (!bfinal_transcript) {
            bmessage.innerHTML = 'Click "Start" and begin speaking.';
            bisbn_button.innerHTML = 'By &nbsp; ISBN Number';
            return;
        }
    };

    recognition.onerror = function (event) {
        if (event.error == 'no-speech') {
            message.innerHTML = 'No speech was detected.';
            ignore_onend = true;
        }
        if (event.error == 'audio-capture') {
            message.innerHTML = 'No microphone was found. Ensure that a microphone is installed.';
            ignore_onend = true;
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - start_timestamp < 100) {
                message.innerHTML = 'Permission to use microphone is blocked. To change, go to chrome://settings/contentExceptions#media-stream';
            } else {
                message.innerHTML = 'Permission to use microphone was denied.';
            }
            ignore_onend = true;
        }
    };

        brecognition.onerror = function (event) {
        if (event.error == 'no-speech') {
            bmessage.innerHTML = 'No speech was detected.';
            bignore_onend = true;
        }
        if (event.error == 'audio-capture') {
            bmessage.innerHTML = 'No microphone was found. Ensure that a microphone is installed.';
            bignore_onend = true;
        }
        if (event.error == 'not-allowed') {
            if (event.timeStamp - start_timestamp < 100) {
                bmessage.innerHTML = 'Permission to use microphone is blocked. To change, go to chrome://settings/contentExceptions#media-stream';
            } else {
                bmessage.innerHTML = 'Permission to use microphone was denied.';
            }
            bignore_onend = true;
        }
    };

}

function talkWithApp(event) {
    final_transcript = '';
    recognition.lang = language.value;
    recognition.start();
    ignore_onend = false;
    final_span.innerHTML = '';
    interim_span.innerHTML = '';
    parse_span.innerHTML = '';
    message.innerHTML = 'Click the "Allow" button above to enable your microphone.';
    start_timestamp = event.timeStamp;
}

function stopButton(event) {
    if (recognizing) {
        recognition.stop();
        message.innerHTML = 'You Stopped the Recognition! Click Start';
        talk_button.innerHTML = 'Start';
        
        return;
    }
}

function bstopButton(event) {
    if (recognizing) {
        brecognition.stop();
        message.innerHTML = 'You Stopped the Recognition! Click Start';
        btalk_button.innerHTML = 'Search By title';
        bisbn_button.innerHTML = 'By  ISBN';
        return;
    }
}

function speakButton(event) {
        recognition.stop();
        message.innerHTML = 'Click "Start" and begin speaking.';
        talk_button.innerHTML = 'Start';
        speechMyText(final_transcript);
        return;
}

function clearButton(event) {
        if (recognizing) {
        recognition.stop();
    }
        final_span.innerHTML = '';
        interim_span.innerHTML = '';
        parse_span.innerHTML = '';
        ngram_span.innerHTML = '';
        tense_span.innerHTML = '' ;
        name_span.innerHTML = '';

        return;
}

function bclearButton(event) {
        if (recognizing) {
        recognition.stop();
    }
        
        binterim_span.innerHTML = '';
        document.getElementById("ptext").placeholder = '';
       
        return;
}

// Speech Synthesis
function speechMyText(textToSpeech) {
    var u = new SpeechSynthesisUtterance();
    u.text = textToSpeech;
    u.lang = language.value;
    u.rate = 1.0;
    u.onend = function (event) {}
    speechSynthesis.speak(u);
}

function nGrams(sentence, n) {
  // Splitting sentence  into words
  var words = sentence.split(/\W+/);
  // Total number of n-grams we will have
  var total = words.length - n;
  var grams = [];
  // Loop through and create all sequences
  for(var i = 0; i <= total; i++) {
    var seq = '';
    for (var j = i; j < i + n; j++) {
      seq += words[j] + ' ';
    }
    // Add to array
    grams.push(seq);
  }
  return grams;
}

function Searchwithtitle(event) {
    bfinal_transcript = '';
    brecognition.lang = blanguage.value;
    brecognition.start();
    bignore_onend = false;
    binterim_span.innerHTML = '';
    bmessage.innerHTML = 'Click the "Allow" button above to enable your microphone.';
    bstart_timestamp = event.timeStamp;
}

function getBookbytitle(title) {
  
  
  var url = "https://www.googleapis.com/books/v1/volumes?q=title:" + title;
  window.location.replace(url);
   
}